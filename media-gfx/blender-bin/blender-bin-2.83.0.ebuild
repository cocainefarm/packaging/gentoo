# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit desktop xdg-utils

PVS="${PV/.0/}"

DESCRIPTION="Blender"
HOMEPAGE="https://www.blender.org/"
SRC_URI="https://ftp.halifax.rwth-aachen.de/blender/release/Blender${PVS}/blender-${PV}-linux64.tar.xz -> blender-${PV}-linux64.tar.xz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/blender-${PV}-linux64"

src_install() {
	local BLENDER_HOME=/opt/${PN}

	# Install firefox in /opt
	dodir ${BLENDER_HOME%/*}
	mv "${S}" "${D}"${BLENDER_HOME} || die
	cd "${WORKDIR}" || die

	make_desktop_entry "${BLENDER_HOME}/blender" "Blender" "blender" "Graphics;3DGraphics;"
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}
