EAPI=7

inherit desktop xdg-utils

DESCRIPTION="Launch a new emacsclient"
HOMEPAGE=""
SRC_URI=""

LICENSE="MIT"
KEYWORDS="*"
SLOT="0"
IUSE=""

RDEPEND=""

S="${WORKDIR}"

src_prepare() {
	default

	mkdir -p ${WORKDIR}
}

src_install() {
	make_desktop_entry "/usr/bin/emacsclient -nc" "emacsclient" "emacs" "Development;TextEditor;"
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}
