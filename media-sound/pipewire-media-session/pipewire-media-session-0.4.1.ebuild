# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson-multilib

DESCRIPTION="Example session manager for PipeWire"
HOMEPAGE="https://gitlab.freedesktop.org/pipewire/media-session"
SRC_URI="https://gitlab.freedesktop.org/pipewire/media-session/-/archive/${PV}/media-session-${PV}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~ppc ~ppc64 ~riscv ~sparc ~x86"
IUSE=""
IUSE="doc systemd test"

BDEPEND="
	>=dev-util/meson-0.59
	virtual/pkgconfig
	doc? (
		app-doc/doxygen
		media-gfx/graphviz
	)
"

RDEPEND="
	sys-apps/dbus[${MULTILIB_USEDEP}]
	virtual/libintl[${MULTILIB_USEDEP}]
	virtual/libudev[${MULTILIB_USEDEP}]
	systemd? ( sys-apps/systemd )
"

DEPEND="${RDEPEND}"

S="${WORKDIR}/media-session-${PV}"

multilib_src_configure() {
	local emesonargs=(
		$(meson_native_use_feature doc docs)
		$(meson_feature test tests)
		$(meson_native_use_feature systemd)
		-Dsystemd-system-service=disabled # Matches upstream
		$(meson_native_use_feature systemd systemd-user-service)
	)

	meson_src_configure
}

multilib_src_install() {
	# Our custom DOCS do not exist in multilib source directory
	DOCS= meson_src_install
}
