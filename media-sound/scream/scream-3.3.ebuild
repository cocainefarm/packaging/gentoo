EAPI=7

DESCRIPTION="Receiver for the virtual network sound card for Microsoft Windows"
HOMEPAGE="https://github.com/duncanthrax/scream"
SRC_URI="
 amd64? ( https://github.com/duncanthrax/scream/archive/${PV}.tar.gz )"

LICENSE="MS-PL"
KEYWORDS="amd64"
SLOT="0"
IUSE="alsa pulseaudio ivshmem"
REQUIRED_USE="|| ( alsa pulseaudio )"

RDEPEND="
	alsa? ( media-libs/alsa-lib )
	pulseaudio? ( media-sound/pulseaudio )"

src_compile() {
	pushd Receivers

	if use alsa ; then
		elog "Compiling alsa client"
		pushd alsa
		emake
		popd

		if use ivshmem ; then
			elog "Compiling alsa-ivshmem client"
			pushd alsa-ivshmem
			emake
			popd
		fi
	fi

	if use pulseaudio ; then
		elog "Compiling pulseaudio client"
		pushd pulseaudio
		emake
		popd

		if use ivshmem ; then
			elog "Compiling pulseaudio-ivshmem client"
			pushd pulseaudio-ivshmem
			emake
			popd
		fi
	fi

	popd
}

src_install() {
	if use alsa ; then
		dobin Receivers/alsa/scream-alsa
		if use ivshmem ; then
			dobin Receivers/alsa/scream-alsa-ivshmem
		fi
	fi

	if use pulseaudio ; then
		dobin Receivers/pulseaudio/scream-pulse
		if use ivshmem ; then
			dobin Receivers/pulseaudio/scream-pulse-ivshmem
		fi
	fi

	einstalldocs
}
