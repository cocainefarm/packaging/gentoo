# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 desktop

DESCRIPTION="Virtual Eurorack DAW"
HOMEPAGE="https://vcvrack.com/"
# SRC_URI="https://github.com/VCVRack/Rack/archive/v${PV}.tar.gz"
EGIT_REPO_URI="https://github.com/VCVRack/Rack.git"
EGIT_COMMIT="v${PV}"

LICENSE="GPLv3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RESTRICT="network-sandbox"

DEPEND="
	x11-libs/libX11
	x11-libs/libXrandr
	x11-libs/libXinerama
	x11-libs/libXcursor
	x11-libs/libXi
	sys-libs/zlib
	dev-libs/libzip
	media-libs/speexdsp
	media-libs/libsamplerate
	media-libs/rtaudio
	media-libs/alsa-lib
	media-libs/portmidi
	media-libs/glew
	>=media-libs/glfw-3.3.2
	dev-libs/jansson
	x11-libs/gtk+
	media-sound/jack2
"

RDEPEND="${DEPEND}"
BDEPEND="
	app-misc/jq
"

src_prepare() {
	pushd dep
	emake rtmidi-4.0.0
	popd

	default

	eapply -p0 "${FILESDIR}/10-link-to-syslibs.patch"
	eapply_user
}

src_compile() {
	pushd dep
	emake lib/librtmidi.a
	emake include/nanovg.h
	emake include/nanosvg.h
	emake include/blendish.h
	emake include/osdialog.h
	emake include/pffft.h
	popd

	emake
}

src_install() {
	dodir /opt/${PN}

	newicon -s 48 icon.ico vcv-rack.ico
	make_desktop_entry "/opt/vcv-rack/Rack" "VCV Rack" "vcv-rack" "AudioVideo;Audio;AudioVideoEditing" "Path=/opt/${PN}"

	dodoc LICENSE-dist.txt LICENSE-GPLv3.txt LICENSE.md CHANGELOG.md

	exeinto /opt/${PN}
	doexe Rack

	insinto /opt/${PN}
	doins -r Core.json cacert.pem res
}
