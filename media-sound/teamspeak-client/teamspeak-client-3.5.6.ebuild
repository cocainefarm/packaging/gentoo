# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit desktop toolchain-funcs unpacker xdg-utils

DESCRIPTION="Some words here"
HOMEPAGE="https://www.teamspeak.com/"
SRC_URI="
 x86? ( https://files.teamspeak-services.com/releases/client/${PV}/TeamSpeak3-Client-linux_x86-${PV}.run -> ${P}-x86.run )
 amd64? ( https://files.teamspeak-services.com/releases/client/${PV}/TeamSpeak3-Client-linux_amd64-${PV}.run -> ${P}-amd64.run )"

LICENSE="teamspeak3 || ( GPL-2 GPL-3 LGPL-3 )"
KEYWORDS="-* amd64 x86"
SLOT="0"
IUSE="alsa pulseaudio"
REQUIRED_USE="|| ( alsa pulseaudio )"

RDEPEND="
	virtual/udev
	alsa? ( media-libs/alsa-lib )
	pulseaudio? ( media-sound/pulseaudio )"

S="${WORKDIR}"

src_prepare() {
	default

	if ! use alsa; then
		rm -f soundbackends/libalsa_linux_*.so || die
	fi

	if ! use pulseaudio ; then
		rm -f soundbackends/libpulseaudio_linux_*.so || die
	fi

	ln -s ts3client_runscript.sh ts3client
}

src_install() {
	exeinto /opt/teamspeak3-client-bin
	doexe error_report package_inst ts3client ts3client* update *.so* QtWebEngineProcess

	exeinto /opt/teamspeak3-client-bin/soundbackends
	doexe soundbackends/*.so

	exeinto /opt/teamspeak3-client-bin/sqldrivers
	doexe sqldrivers/*.so

	exeinto /opt/teamspeak3-client-bin/iconengines
	doexe iconengines/*.so

	exeinto /opt/teamspeak3-client-bin/imageformats
	doexe imageformats/*.so

	exeinto /opt/teamspeak3-client-bin/platforms
	doexe platforms/*.so

	exeinto /opt/teamspeak3-client-bin/xcbglintegrations
	doexe xcbglintegrations/*.so

	insinto /opt/teamspeak3-client-bin
	doins -r gfx html resources sound styles translations qt.conf openglblacklist.json qtwebengine_locales

	make_desktop_entry /opt/teamspeak3-client-bin/ts3client "Teamspeak 3 Client" /opt/teamspeak3-client-bin/styles/default/logo-128x128.png "Audio;AudioVideo;Network"

	einstalldocs
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}

