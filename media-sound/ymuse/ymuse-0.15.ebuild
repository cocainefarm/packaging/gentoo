EAPI=7
inherit go-module desktop

DESCRIPTION="GTK client for Music Player Daemon (MPD) written in Go"
HOMEPAGE="https://github.com/yktoo/ymuse"

EGO_SUM=(
  "github.com/fhs/gompd/v2 v2.1.2-0.20200903175203-c269f23a98e3"
  "github.com/fhs/gompd/v2 v2.1.2-0.20200903175203-c269f23a98e3/go.mod"
  "github.com/gotk3/gotk3 v0.4.1-0.20200810205902-010a4368f09d"
  "github.com/gotk3/gotk3 v0.4.1-0.20200810205902-010a4368f09d/go.mod"
  "github.com/op/go-logging v0.0.0-20160315200505-970db520ece7"
  "github.com/op/go-logging v0.0.0-20160315200505-970db520ece7/go.mod"
  "github.com/pkg/errors v0.9.1"
  "github.com/pkg/errors v0.9.1/go.mod"
)
go-module_set_globals
SRC_URI="https://github.com/yktoo/ymuse/archive/v${PV}.tar.gz -> ${P}.tar.gz
	${EGO_SUM_SRC_URI}"

LICENSE="Apache-2.0"
KEYWORDS="amd64"
SLOT="0"

RDEPEND=""

src_prepare() {
	default

	go generate
}

src_compile() {
	default

	go build
}

src_install() {
	default

	dobin ${PN}
	domenu resources/ymuse.desktop

	insinto /usr/share/icons
	doins -r resources/icons

	insinto /usr/share/locale
	doins -r resources/i18n/generated
}
