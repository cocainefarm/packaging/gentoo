# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

MULTILIB_COMPAT=( abi_x86_{32,64} )

inherit unpacker multilib-minimal

SUPER_PN='amdgpu-pro'
MY_PV=$(ver_rs 2 '-')

DESCRIPTION="Proprietary OpenCL implementation for AMD GPUs"
HOMEPAGE="https://www.amd.com/en/support/kb/release-notes/rn-amdgpu-unified-linux-20-40"
SRC_URI="${SUPER_PN}-${MY_PV}-ubuntu-20.04.tar.xz"

LICENSE="AMD-GPU-PRO-EULA"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE="legacy +pal"

RESTRICT="bindist mirror fetch strip"

BDEPEND="dev-util/patchelf"
COMMON=">=virtual/opencl-3"
DEPEND="${COMMON}"
RDEPEND="${COMMON}
	!media-libs/mesa[opencl]" # Bug #686790

QA_PREBUILT=(
	"/opt/amdgpu/lib*/*"
	"/opt/amdgpu-pro/lib*/*"
)

S="${WORKDIR}/${SUPER_PN}-${MY_PV}-ubuntu-20.04"

pkg_nofetch() {
	local pkgver=$(ver_cut 1-2)
	einfo "Please download Radeon Software for Linux version ${pkgver} for Ubuntu 20.04.1 from"
	einfo "    ${HOMEPAGE}"
	einfo "The archive should then be placed into your distfiles directory."
}

src_unpack() {
	default

	local patchlevel=$(ver_cut 3)

	cd "${S}" || die

	unpack_deb "${S}/amdgpu-pro-core_${MY_PV}_all.deb"

	if use legacy ; then
		unpack_deb "${S}/libdrm-amdgpu-common_1.0.0-${patchlevel}_all.deb"
	fi

	multilib_parallel_foreach_abi multilib_src_unpack
}

multilib_src_unpack() {
	local libdrm_ver="2.4.100"
	local patchlevel=$(ver_cut 3)
	local deb_abi
	[[ ${ABI} == x86 ]] && deb_abi=i386

	mkdir -p "${BUILD_DIR}" || die
	pushd "${BUILD_DIR}" >/dev/null || die

	unpack_deb "${S}/ocl-icd-libopencl1-amdgpu-pro_${MY_PV}_${deb_abi:-${ABI}}.deb"

	if use legacy ; then
		unpack_deb "${S}/libdrm-amdgpu-amdgpu1_${libdrm_ver}-${patchlevel}_${deb_abi:-${ABI}}.deb"
		unpack_deb "${S}/libdrm2-amdgpu_${libdrm_ver}-${patchlevel}_${deb_abi:-${ABI}}.deb"
		unpack_deb "${S}/opencl-orca-amdgpu-pro-icd_${MY_PV}_${deb_abi:-${ABI}}.deb"
	fi

	if use pal ; then
		unpack_deb "${S}/opencl-amdgpu-pro-comgr_${MY_PV}_${deb_abi:-${ABI}}.deb"
		unpack_deb "${S}/opencl-amdgpu-pro-icd_${MY_PV}_${deb_abi:-${ABI}}.deb"
	fi

	popd >/dev/null || die
}

multilib_src_install() {
	local dir_abi short_abi
	[[ ${ABI} == x86 ]] && dir_abi=i386-linux-gnu && short_abi=32
	[[ ${ABI} == amd64 ]] && dir_abi=x86_64-linux-gnu && short_abi=64

	into "/opt/amdgpu-pro"

#	if use legacy ; then
#		patchelf --set-rpath '$ORIGIN' "opt/${SUPER_PN}/lib/${dir_abi}"/libamdocl-orca${short_abi}.so || die "Failed to fix library rpath"
#	fi
#
#	if use pal ; then
#		patchelf --set-rpath '$ORIGIN' "opt/${SUPER_PN}/lib/${dir_abi}"/libamdocl${short_abi}.so || die "Failed to fix library rpath"
#	fi

	dolib.so "opt/amdgpu-pro/lib/${dir_abi}"/*

	if use legacy ; then
		into "/opt/amdgpu"
		dolib.so "opt/amdgpu/lib/${dir_abi}"/*
	fi


	insinto "/etc/OpenCL/vendors"

	if use legacy ; then
		echo "/opt/amdgpu-pro/$(get_libdir)/libamdocl-orca${short_abi}.so" \
				> "etc/OpenCL/vendors/amdocl-orca${short_abi}.icd" || die "Failed to generate ICD file for ABI ${ABI}"
		doins "etc/OpenCL/vendors/amdocl-orca${short_abi}.icd"
	fi

	if use pal ; then
		echo "/opt/amdgpu-pro/$(get_libdir)/libamdocl${short_abi}.so" \
			> "etc/OpenCL/vendors/amdocl${short_abi}.icd" || die "Failed to generate ICD file for ABI ${ABI}"
		doins "etc/OpenCL/vendors/amdocl${short_abi}.icd"
	fi


	if use legacy ; then
		insinto "/lib/udev/rules.d/"
		doins "lib/udev/rules.d/91-amdgpu-pro-modeset.rules"
	fi

	docompress -x "/usr/share/doc/${PF}/ocl-icd-libopencl1-amdgpu-pro/changelog.Debian.gz"

	if use legacy ; then
		docompress -x "/usr/share/doc/${PF}/libdrm-amdgpu-amdgpu1/changelog.Debian.gz"
		docompress -x "/usr/share/doc/${PF}/libdrm2-amdgpu/changelog.Debian.gz"
		docompress -x "/usr/share/doc/${PF}/opencl-orca-amdgpu-pro-icd/changelog.Debian.gz"
	fi

	if use pal ; then
		docompress -x "/usr/share/doc/${PF}/opencl-amdgpu-pro-comgr/changelog.Debian.gz"
		docompress -x "/usr/share/doc/${PF}/opencl-amdgpu-pro-icd/changelog.Debian.gz"
	fi

	dodoc -r "usr/share/doc/ocl-icd-libopencl1-amdgpu-pro"

	if use legacy ; then
		dodoc -r "usr/share/doc/libdrm-amdgpu-amdgpu1"
		dodoc -r "usr/share/doc/libdrm2-amdgpu"
		dodoc -r "usr/share/doc/opencl-orca-amdgpu-pro-icd"
	fi

	if use pal ; then
		dodoc -r "usr/share/doc/opencl-amdgpu-pro-comgr"
		dodoc -r "usr/share/doc/opencl-amdgpu-pro-icd"
	fi
}

multilib_src_install_all() {
	if use legacy ; then
		insinto "/opt/amdgpu/share/libdrm"
		doins "opt/amdgpu/share/libdrm/amdgpu.ids"
	fi

	docompress -x "/usr/share/doc/${PF}/amdgpu-pro-core/changelog.Debian.gz"

	if use legacy ; then
		docompress -x "/usr/share/doc/${PF}/libdrm-amdgpu-common/changelog.Debian.gz"
		doins "opt/amdgpu/share/libdrm/amdgpu.ids"
	fi

	dodoc -r "usr/share/doc/amdgpu-pro-core"

	if use legacy ; then
		dodoc -r "usr/share/doc/libdrm-amdgpu-common"
	fi

}

pkg_postinst() {
	if [[ -z "${REPLACING_VERSIONS}" ]]; then
		ewarn "Please note that using proprietary OpenCL libraries together with the"
		ewarn "Open Source amdgpu stack is not officially supported by AMD. Do not ask them"
		ewarn "for support in case of problems with this package."
		ewarn ""
		ewarn "Furthermore, if you have the whole AMDGPU-Pro stack installed this package"
		ewarn "will almost certainly conflict with it. This might change once AMDGPU-Pro"
		ewarn "has become officially supported by Gentoo."
	fi

	elog ""
	elog "This package is now DEPRECATED on amd64 in favour of dev-libs/rocm-opencl-runtime."
	elog ""
}
