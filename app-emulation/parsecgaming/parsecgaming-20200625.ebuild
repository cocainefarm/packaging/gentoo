# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit desktop xdg-utils

DESCRIPTION="Parsec"
HOMEPAGE="http://parsec.tv"
SRC_URI="https://builds.parsecgaming.com/package/parsec-linux.deb -> parsec-linux.deb"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
	>=x11-libs/cairo-1.6.0
	>=media-libs/freetype-2.2.1
	>=sys-devel/gcc-3.0
	>=x11-libs/gdk-pixbuf-2.22.0
	media-libs/mesa
	>=dev-libs/glib-2.24.0
	>=x11-libs/gtk+-2.24.0:2
	>=x11-libs/pango-1.22.0
	x11-libs/libSM
	>=media-libs/libao-1.1.0
	x11-libs/libX11
	x11-libs/libXxf86vm
"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}"

src_prepare() {
	default

	tar xfv "${S}/data.tar.xz"
}

src_install() {
	doins -r usr
	fperms +x /usr/bin/parsecd
	fperms +x /usr/share/parsec/skel/parsecd-150-28.so
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}
