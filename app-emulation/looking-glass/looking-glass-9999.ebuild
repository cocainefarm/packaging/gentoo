# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

MY_PN="LookingGlass"
MY_PV="a11"
EGIT_SUBMODULES=( 'repos/PureSpice' 'LGMP' )
EGIT_REPO_URI="https://github.com/gnif/${MY_PN}.git"

inherit cmake-utils git-r3

DESCRIPTION="A low latency KVM FrameRelay implementation for guests with VGA PCI Passthrough"
HOMEPAGE="https://looking-glass.hostfission.com https://github.com/gnif/LookingGlass/"
SRC_URI=""

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="debug +host"

RDEPEND="dev-libs/libconfig:0=
 dev-libs/nettle:=[gmp]
 media-libs/freetype:2
 media-libs/fontconfig:1.0
 media-libs/libsdl2
 media-libs/sdl2-ttf
 virtual/glu"
DEPEND="${RDEPEND}
 app-emulation/spice-protocol
 virtual/pkgconfig"

CMAKE_USE_DIR="${S}"/client

src_prepare() {
 default

 # Respect FLAGS
 # A bug is caused when compiling with optimisations so we need to set -O0 here
 # https://github.com/gnif/PureSpice/issues/1
 filter-flags -O3 -O2 -O1
 sed -i '/CMAKE_C_FLAGS/s/-O3/-O0/' \
  client/CMakeLists.txt || die "sed failed for FLAGS"

 if ! use debug ; then
  sed -i '/CMAKE_C_FLAGS/s/-g //' \
  client/CMakeLists.txt || die "sed failed for debug"
 fi

 cmake-utils_src_prepare
}

src_install() {
 einstalldocs

 dobin "${BUILD_DIR}"/looking-glass-client

 newicon -s 128 resources/icon-128x128.png looking-glass.png

 make_desktop_entry /usr/bin/looking-glass-client "LookingGlass Client" /usr/share/icons/128x128/apps/looking-glass.png "System"
 make_desktop_entry "/usr/bin/looking-glass-client -s" "LookingGlass Client (SPICE Disabled)" /usr/share/icons/128x128/apps/looking-glass.png "System"
}

pkg_postinst() {
		xdg_desktop_database_update
		xdg_mimeinfo_database_update
}

pkg_postrm() {
		xdg_desktop_database_update
		xdg_mimeinfo_database_update
}
