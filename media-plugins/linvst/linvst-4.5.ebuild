# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit multilib

DESCRIPTION="Linux Windows vst wrapper/bridge"
HOMEPAGE="https://github.com/osxmidi/LinVst"
SRC_URI="https://github.com/osxmidi/LinVst/archive/refs/tags/${PV}.tar.gz -> linvst-${PV}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="
  virtual/wine
  x11-libs/libX11
"

RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}/LinVst-${PV}"

src_compile() {
	emake DESTDIR="${D}" VST_DIR="${D}/usr/$(get_libdir)/vst"
}

src_install() {
	emake DESTDIR="${D}" VST_DIR="${D}/usr/$(get_libdir)/vst" install
	dodoc README.md
}
